import heapq
from collections import defaultdict
import os
import json_obz2
import matplotlib.pyplot as plt
import json, random, string, sys
import comp_form

# def huffman_encoding(data):
#     # Step 1: Build a frequency dictionary
#     freq_dict = defaultdict(int)
#     for char in data:
#         freq_dict[char] += 1
#
#     # Step 2: Build a priority queue of nodes
#     heap = [[freq, [char, ""]] for char, freq in freq_dict.items()]
#     heapq.heapify(heap)
#
#     # Step 3: Build a binary tree from the priority queue
#     while len(heap) > 1:
#         left = heapq.heappop(heap)
#         right = heapq.heappop(heap)
#         for pair in left[1:]:
#             pair[1] = '0' + pair[1]
#         for pair in right[1:]:
#             pair[1] = '1' + pair[1]
#         heapq.heappush(heap, [left[0] + right[0]] + left[1:] + right[1:])
#
#     # Step 4: Build the encoding dictionary
#     encoding_dict = dict(heapq.heappop(heap)[1:])
#
#     # Step 5: Encode the data
#     encoded_data = ""
#     for char in data:
#         encoded_data += encoding_dict[char]
#
#     return encoded_data, encoding_dict
#
# def huffman_decoding(encoded_data, encoding_dict):
#     # Invert the encoding dictionary
#     decoding_dict = {v: k for k, v in encoding_dict.items()}
#
#     # Decode the encoded data
#     decoded_data = ""
#     code = ""
#     for bit in encoded_data:
#         code += bit
#         if code in decoding_dict:
#             decoded_data += decoding_dict[code]
#             code = ""
#
#     return decoded_data

def generate_json_file(file_path, target_size):
    """
    Generate a JSON file with the specified size.

    Args:
        file_path (str): The path of the file to write the JSON data to.
        target_size (int): The target size of the JSON file in bytes.
    """
    obj = {}

    # Keep adding random keys and values until the target size is reached.
    while True:
        # Generate a random key.
        key = ''.join(random.choices(string.ascii_lowercase, k=5))

        # Generate a random value (string, int, or float).
        value_type = random.choice([str, int, float])
        value = value_type(random.random() * 100)

        # Add the key-value pair to the object.
        obj[key] = value

        # Check if the current JSON string is equal to or larger than the target size.
        json_str = json.dumps(obj)
        if len(json_str) >= target_size:
            # Truncate the JSON string to the target size and write it to the file.
            break

    last_colon_index = json_str.rfind(":")  # find the index of the last ":"
    json_str_normal = json_str[:last_colon_index]  # remove all characters after the last ":"
    #
    json_str_normal = json_str_normal + ": ghbdtn"
    with open(file_path, 'a') as f:
        f.write(json_str_normal + '\n')

def add_character(character_list, new_character):
    character_list.append(new_character)
#v
# x = []
# y = []
#
# x1 = []
# y1 = []
#
# x2 = []
# y2 = []
#
# x3 = []
# y3 = []
#
# x4 = []
# y4 = []
#
# for i in range (30, 2048, 10):
#     generate_json_file('json.txt', i)
#     # Open the input file in read mode
#     with open('json.txt', 'r') as f:
#         # Read the contents of the file
#         text = f.read()
#
#     # Convert the text to binary representation
#     binary_text = ''.join(format(ord(c), '08b') for c in text)
#
#     # Open the output file in write mode
#     with open('3.txt', 'w') as f:
#         # Write the binary text to the file
#         f.write(binary_text)
#
#     # Read text from file
#     with open("json.txt", "r") as file:
#         data = file.read()
#
#     # Encode the data
#     encoded_data, encoding_dict = huffman_encoding(data)
#
#     # Write encoded text to file
#     with open("2.txt", "w") as file:
#         file.write(encoded_data)
#
#     # Compare file sizes
#     input_size = os.path.getsize("3.txt") / 8
#     output_size = os.path.getsize("2.txt") / 8
#     compression_ratio = input_size/output_size
#     print("Input file size:", input_size, "bytes")
#     print("Output file size:", output_size, "bytes")
#     print("Compression ratio: {:.2f}%".format((1 - output_size / input_size) * 100))
#
#     # Decode the encoded data
#     decoded_data = huffman_decoding(encoded_data, encoding_dict)
#
#     # Check if decoding is correct
#     print("Decoded data matches input data:", decoded_data == data)
#
#     input_file_size_zip, output_file_size_zip = comp_form.compress_file_zip("json.txt")
#     add_character(x1, input_file_size_zip)
#     add_character(y1, input_file_size_zip / output_file_size_zip)
#
#     input_file_size_zlib, output_file_size_zlib = comp_form.compress_file_zlib("json.txt")
#     add_character(x2, input_file_size_zlib)
#     add_character(y2, input_file_size_zlib / output_file_size_zlib)
#
#     input_file_size_7z, output_file_size_7z = comp_form.compress_file_7z("json.txt")
#     add_character(x3, input_file_size_7z)
#     add_character(y3, input_file_size_7z / output_file_size_7z)
#
#     input_file_size_bzip2, output_file_size_bzip2 = comp_form.compress_file_bzip2("json.txt")
#     add_character(x4, input_file_size_bzip2)
#     add_character(y4, input_file_size_bzip2 / output_file_size_bzip2)
#
#     add_character(x, input_size)
#     add_character(y, compression_ratio)
#
# # Create a figure and axis
# fig, ax = plt.subplots()
#
# # Plot the data
# ax.plot(x, y, label='Huffman')
# ax.plot(x1, y1, label='ZIP')
# ax.plot(x2, y2, label='ZLIB')
# ax.plot(x3, y3, label='7Z')
# ax.plot(x4, y4, label='BZIP2')
#
# # Set the labels
# ax.set_xlabel('Input size in bytes')
# ax.set_ylabel('Compression ratio')
# ax.set_title('Types of data encoding')
#
# # Add a legend
# ax.legend()
#
# # Show the plot
# plt.show()

