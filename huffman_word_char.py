import json
import heapq
import os
import all_algorythma
import matplotlib.pyplot as plt
import time
import numpy as np
from scipy import stats
from collections import Counter


def build_huffman_tree(freq_dict):
    heap = [[weight, [key, '']] for key, weight in freq_dict.items()]
    heapq.heapify(heap)
    while len(heap) > 1:
        low = heapq.heappop(heap)
        high = heapq.heappop(heap)
        for pair in low[1:]:
            pair[1] = '0' + pair[1]
        for pair in high[1:]:
            pair[1] = '1' + pair[1]
        heapq.heappush(heap, [low[0] + high[0]] + low[1:] + high[1:])
    encoding_dict = dict(sorted(heapq.heappop(heap)[1:], key=lambda x: (len(x[-1]), x)))
    decoding_dict = {v: k for k, v in encoding_dict.items()}
    return encoding_dict, decoding_dict

def huffman_encoding(data):
    freq_dict = {}
    for char in data:
        if char not in freq_dict:
            freq_dict[char] = 1
        else:
            freq_dict[char] += 1
    encoding_dict, decoding_dict = build_huffman_tree(freq_dict)
    encoded_data = ''.join([encoding_dict[char] for char in data])
    return encoded_data, encoding_dict, decoding_dict

def huffman_decoding(encoded_data, decoding_dict):
    decoded_data = ''
    i = 0
    while i < len(encoded_data):
        for k in decoding_dict.keys():
            if encoded_data[i:].startswith(k):
                decoded_data += decoding_dict[k]
                i += len(k)
                break
    return decoded_data

def build_huffman_tree_word(freq_dict):
    heap = [[weight, [key, '']] for key, weight in freq_dict.items()]
    heapq.heapify(heap)
    while len(heap) > 1:
        low = heapq.heappop(heap)
        high = heapq.heappop(heap)
        for pair in low[1:]:
            pair[1] = '0' + pair[1]
        for pair in high[1:]:
            pair[1] = '1' + pair[1]
        heapq.heappush(heap, [low[0] + high[0]] + low[1:] + high[1:])
    encoding_dict = dict(sorted(heapq.heappop(heap)[1:], key=lambda x: (len(x[-1]), x)))
    decoding_dict = {v: k for k, v in encoding_dict.items()}
    return encoding_dict, decoding_dict

def huffman_encoding_word(data):
    freq_dict = {}
    words = data.split()
    for word in words:
        if word not in freq_dict:
            freq_dict[word] = 1
        else:
            freq_dict[word] += 1
    encoding_dict, decoding_dict = build_huffman_tree_word(freq_dict)
    encoded_data = ''
    for word in words:
        encoded_data += encoding_dict[word]
    return encoded_data, encoding_dict

def huffman_decoding_word(encoded_data, encoding_dict):
    current_code = ''
    decoded_data = ''
    decoding_dict = {v: k for k, v in encoding_dict.items()}
    for bit in encoded_data:
        current_code += bit
        if current_code in decoding_dict:
            decoded_data += decoding_dict[current_code] + " "
            current_code = ''
    return decoded_data

x = []
y = []
col = 0
colich = 10
while col < colich:
    for i in range (100, 2040, 10):
        all_algorythma.generate_json_file('json.txt', i)
        # Read JSON file
        with open('json.txt') as f:
            data = json.load(f)

        # Write keys to key file and values to value file
        with open('keys.txt', 'w') as keys_file, open('values.txt', 'w') as values_file:
            for key, value in data.items():
                keys_file.write(key + ' ')
                values_file.write(str(value) + ' ')

        # Example usage
        filename = 'values.txt'
        with open(filename, 'r') as file:
            data = file.read()

        # Convert the text to binary representation
        binary_text = ''.join(format(ord(c), '08b') for c in data)

        # Open the output file in write mode
        with open('3.txt', 'w') as f:
            # Write the binary text to the file
            f.write(binary_text)

        encoded_data, encoding_dict, decoding_dict = huffman_encoding(data)

        encoded_filename = 'values_encoded.txt'
        with open(encoded_filename, 'w') as file:
            file.write(encoded_data)

        with open(encoded_filename, 'r') as file:
            encoded_data = file.read()

        decoded_data = huffman_decoding(encoded_data, decoding_dict)

        decoded_filename = 'values_decoded.txt'
        with open(decoded_filename, 'w') as file:
            file.write(decoded_data)

        with open(decoded_filename, 'r') as file:
            decoded_data = file.read()

        if data == decoded_data:
            print('Values match')
        else:
            print("Values don't match")

        input_size_values = os.path.getsize("3.txt") / 8
        output_size_values = os.path.getsize("values_encoded.txt") / 8
        compression_ratio_values = input_size_values / output_size_values
        print("Input file size:", input_size_values, "bytes")
        print("Output file size:", output_size_values, "bytes")
        print("Compression ratio", compression_ratio_values)

    #print (data)
    #print (decoded_data)

    # Example usage
        with open('keys.txt', 'r') as file:
            data = file.read()

    # Convert the text to binary representation
        binary_text = ''.join(format(ord(c), '08b') for c in data)

    # Open the output file in write mode
        with open('3.txt', 'w') as f:
        # Write the binary text to the file
            f.write(binary_text)

        encoded_data, encoding_dict = huffman_encoding_word(data)
        with open('keys_encoded.txt', 'w') as file:
            file.write(encoded_data)
        decoded_data = huffman_decoding_word(encoded_data, encoding_dict)
        with open('keys_decoded.txt', 'w') as file:
            file.write(decoded_data)
    #print('Original data:', data)
    #print('Encoded data:', encoded_data)
    #print('Decoded data:', decoded_data)
    # print('Keys match', decoded_data == data)
    #print (encoding_dict)
        input_size_keys = os.path.getsize("3.txt") / 8
        output_size_keys = os.path.getsize("keys_encoded.txt") / 8
        compression_ratio_keys = input_size_keys / output_size_keys
        print("Input file size:", input_size_keys, "bytes")
        print("Output file size:", output_size_keys, "bytes")
        print("Compression ratio", compression_ratio_keys)

    # read keys and values from separate files
        with open('keys.txt', 'r') as keys_file:
            keys = keys_file.read().split()

        with open('values.txt', 'r') as values_file:
            values = values_file.read().split()

    # combine keys and values into a dictionary
        data = {}
        for i in range(len(keys)):
            data[keys[i]] = values[i]

    # write combined JSON to a file
        with open('json_result.txt', 'w') as json_file:
            json.dump(data, json_file)

    # start_time = time.time()
    # end_time = time.time()
    #
    # elapsed_time = end_time - start_time
    # print("Elapsed time: {:.2f} seconds".format(elapsed_time))
    # display combined JSON
    #print(json.dumps(data, indent=4))


        with open("json.txt", 'r') as file:
            data = file.read()

    # Convert the text to binary representation
        binary_text = ''.join(format(ord(c), '08b') for c in data)

    # Open the output file in write mode
        with open('3.txt', 'w') as f:
        # Write the binary text to the file
            f.write(binary_text)
        input_size_json = os.path.getsize("3.txt") / 8
        output_size_json = output_size_values + output_size_keys
        compression_ratio_json = input_size_json / output_size_json
        print ("All compression ratio", compression_ratio_json)

        all_algorythma.add_character(x, input_size_json)
        all_algorythma.add_character(y, compression_ratio_json)

    col = col + 1


# fig, ax = plt.subplots()
#
# # Plot the data
# ax.plot(x, y, "o", label='Huffman with dict word and char')
#
# # Set the labels
# ax.set_xlabel('Input size in bytes')
# ax.set_ylabel('Compression ratio')
# ax.set_title('Huffman with dict word and char')
#
# # Add a legend
# ax.legend()
#
# # Show the plot
# plt.show()

expectation = []
std_dev = []

# Perform linear regression
array_length = len(x)
print("Длинна массива", array_length)
print(x)

arr = []
for i in range(100, 2050, 10):
    arr.append(i)
array_length2 = len (arr)
print("Длинна массива", array_length2)
print(arr)


x = np.array(x)
y = np.array(y)
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
line = slope * x + intercept

for i, xi in enumerate(x):
    # Select the corresponding values of y and line
    yi = y[i]
    linei = line[i]

    # Calculate the expectation and standard deviation
    expectation_one = np.mean([yi, linei])
    std_dev_one = np.std([yi, linei])
    all_algorythma.add_character(expectation, expectation_one)
    all_algorythma.add_character(std_dev, std_dev_one)


# Plot the scatter plot and the line of best fit
plt.plot(x, y, 'o', label='data')
plt.plot(x, expectation, 'o', label='Мат ожидание')
plt.plot(x, line, label='Апроксимация')
plt.plot(x, std_dev, 'o', label='Средне квадратичное отклонение')
plt.legend()
plt.show()



